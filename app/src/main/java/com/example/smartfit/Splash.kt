package com.example.smartfit

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import java.lang.Exception

class Splash : AppCompatActivity() {

    private val SPLASH_TIME_OUT:Long=2000 //2 seg
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_splash)
        Handler().postDelayed({
            startActivity(Intent(this,home_smartfit::class.java))
            finish()
        },SPLASH_TIME_OUT)
    }
}
